#!/bin/sh

expand() {
  echo "$(cd "$1" || exit 1; pwd)"
}

ROOT_PATH=$(expand "$(dirname "$0")")
MAIN_COMMAND="$(basename "$0")"

init() {
  PRINT=0
  [ "$1" = "-" ] && PRINT=1

  USER_SHELL="$(basename "$SHELL")"

  if [ "$PRINT" = "0" ] ; then
    case "$USER_SHELL" in
      bash)
        if [ -f "${HOME}/.bashrc" ] && [ ! -f "${HOME}/.bash_profile" ]; then
          profile="$HOME/.bashrc"
        else
          profile="$HOME/.bash_profile"
        fi
        ;;
      zsh )
        profile="$HOME/.zshrc"
        ;;
      ksh )
        profile="$HOME/.profile"
        ;;
      fish )
        profile="$HOME/.config/fish/config.fish"
        ;;
      * )
        profile='your profile'
        ;;
    esac

    { echo "# Load $MAIN_COMMAND automatically by appending"
      echo "# the following to ${profile}:"
      echo "eval \"\$($ROOT_PATH/$MAIN_COMMAND init -)\""
      echo
    } >&2

  exit 1
  fi

  echo "export PATH=\$PATH:$ROOT_PATH"
}

install_linux() {
  if [ -f /etc/os-release ]; then
    . /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
  elif type lsb_release >/dev/null 2>&1; then
    OS=$(lsb_release -si)
    VER=$(lsb_release -sr)
  elif [ -f /etc/lsb-release ]; then
    . /etc/lsb-release
    OS=$DISTRIB_ID
    VER=$DISTRIB_RELEASE
  elif [ -f /etc/debian_version ]; then
    OS=Debian
    VER=$(cat /etc/debian_version)
  else
    echo "Linux distribution not supported (yet)... Need help!"
    exit 1
  fi

  case "$OS" in
    Ubuntu)
      sudo apt install $1
      ;;
    *)
      echo "$OS not supported (yet)... Need help!"
      exit 1
      ;;
  esac
}

install_macos() {
  echo "OSX not supported (yet)... Need help!"
  exit 1
}

install_package() {
  OS=$(uname)
  case $OS in
    'Linux')
      install_linux "$1"
      ;;
    'Darwin')
      install_darwin "$1"
      ;;
    *)
      echo "$OSTYPE not supported (yet)... Need help!"
      exit 1
      ;;
  esac
}

check_package() {
  COMMAND="$1"
  PACKAGE="$2"
  if ! command -v $COMMAND ; then
    echo "$COMMAND not installed..."
    if [ "z$PACKAGE" = "z" ] ; then
      exit 1
    fi
    echo "Installing..."
    install_package "dialog"
  fi
}

DIALOG=${DIALOG=dialog}
MPV=${MPV=mpv}

add_radio() {
  __URL="$1"
  __ONAME="$2"
  __NAME=$(echo "$2" | sed -e 's/ /_/g')
  __GROUP="$3"
  eval "RADIO_$__NAME=\"$__URL\""
}

# fr_FR
add_radio "http://direct.franceinfo.fr/live/franceinfo-midfi.mp3" "France Info" "fr_FR"
add_radio "http://direct.franceinter.fr/live/franceinter-midfi.mp3" "France Inter" "fr_FR"
add_radio "http://direct.francemusique.fr/live/francemusique-midfi.mp3" "France Musique" "fr_FR"
add_radio "http://direct.fipradio.fr/live/fip-midfi.mp3" "FIP" "fr_FR"
add_radio "http://direct.mouv.fr/live/mouv-midfi.mp3" "Le Mouv" "fr_FR"
add_radio "http://direct.franceculture.fr/live/franceculture-midfi.mp3" "France Culture" "fr_FR"
add_radio "http://direct.francebleu.fr/live/fb1071-midfi.mp3" "France Bleu" "fr_FR"
add_radio "http://broadcast.infomaniak.net/jazzradio-high.mp3" "Radio Jazz" "fr_FR"
add_radio "http://cdn.nrjaudio.fm/audio1/fr/30001/mp3_128.mp3?origine=fluxradios" "NRJ" "fr_FR"
add_radio "http://cdn.nrjaudio.fm/audio1/fr/30601/mp3_128.mp3?origine=fluxradios" "Nostalgie" "fr_FR"
add_radio "http://cdn.nrjaudio.fm/audio1/fr/30201/mp3_128.mp3?origine=fluxradios" "Cherie FM" "fr_FR"
add_radio "http://cdn.nrjaudio.fm/audio1/fr/30401/mp3_128.mp3?origine=fluxradios" "Rire et Chansons" "fr_FR"
add_radio "http://streaming.radio.rtl.fr/rtl-1-44-128" "RTL" "fr_FR"
add_radio "http://streaming.radio.rtl2.fr/rtl2-1-44-128" "RTL2" "fr_FR"
add_radio "http://streaming.radio.funradio.fr/fun-1-44-128" "Fun Radio" "fr_FR"
add_radio "http://ais-live.cloud-services.paris:8000/europe1.mp3" "Europe 1" "fr_FR"
add_radio "http://ais-live.cloud-services.paris:8000/virgin.mp3" "Virgin Radio" "fr_FR"
add_radio "https://ais-live.cloud-services.paris:8443/rfm.mp3" "RFM" "fr_FR"
add_radio "http://rmc.bfmtv.com/rmcinfo-mp3" "RMC" "fr_FR"
add_radio "http://chai5she.cdn.dvmr.fr/bfmbusiness" "BFM Business" "fr_FR"
add_radio "http://www.skyrock.fm/stream.php/tunein16_128mp3.mp3" "Skyrock" "fr_FR"
add_radio "http://radioclassique.ice.infomaniak.ch/radioclassique-high.mp3" "Radio Classique" "fr_FR"
add_radio "http://target-ad-1.cdn.dvmr.fr/ouifm-high.aac" "Oui FM" "fr_FR"
add_radio "http://mfm.ice.infomaniak.ch/mfm-128.mp3" "M Radio" "fr_FR"
add_radio "http://francemaghreb2.ice.infomaniak.ch/francemaghreb2-high.mp3" "France Maghreb 2" "fr_FR"
add_radio "http://start-latina.ice.infomaniak.ch/start-latina-high.mp3" "Latina" "fr_FR"
add_radio "http://tsfjazz.ice.infomaniak.ch/tsfjazz-high.mp3" "TSF Jazz" "fr_FR"
add_radio "http://novazz.ice.infomaniak.ch/novazz-128.mp3" "Nova" "fr_FR"
add_radio "http://str0.creacast.com/pharefm" "Phare FM" "fr_FR"
add_radio "http://radiofg.impek.com/fg" "Radio FG" "fr_FR"
add_radio "http://rcf.streamakaci.com/rcf.mp3" "RFC" "fr_FR"
add_radio "http://live02.rfi.fr/rfimonde-96k.mp3" "RFI Monde" "fr_FR"
add_radio "http://start-sud.ice.infomaniak.ch/start-sud-high.mp3" "Sud Radio" "fr_FR"
add_radio "http://start-adofm.ice.infomaniak.ch/start-adofm-high.mp3" "Ado FM" "fr_FR"
add_radio "http://african1paris.ice.infomaniak.ch/african1paris-128.mp3" "Africa N1" "fr_FR"
add_radio "http://alouette.ice.infomaniak.ch/alouette-high.mp3" "Alouette" "fr_FR"
add_radio "http://alpes1grenoble.ice.infomaniak.ch/alpes1grenoble-high.mp3" "Alpes 1 Grand Grenoble" "fr_FR"
add_radio "http://alpes1gap.ice.infomaniak.ch/alpes1gap-high.mp3" "Alpes 1 Alpes du Sud" "fr_FR"
add_radio "http://altafrequenza.streamakaci.com/altafrequenza.mp3" "Alta Frequenza" "fr_FR"
add_radio "http://broadcast.infomaniak.ch/beurfm-high.mp3" "Beur FM" "fr_FR"
add_radio "http://start-blackbox.ice.infomaniak.ch/start-blackbox-high.mp3" "Blackbox" "fr_FR"
add_radio "http://champagnefmaisne.ice.infomaniak.ch/champagnefm-64-aisne.mp3" "Champagne FM Aisne" "fr_FR"
add_radio "http://champagnefm.ice.infomaniak.ch/champagnefm-128-ardennes.mp3" "Champagne FM Ardennes" "fr_FR"
add_radio "http://champagnefm.ice.infomaniak.ch/champagnefm-128-aube.mp3" "Champagne FM Aude" "fr_FR"
add_radio "http://stream1.chantefrance.com/Chante_France" "Chante France" "fr_FR"
add_radio "http://radio-contact.ice.infomaniak.ch/radio-contact-high" "Contact FM" "fr_FR"
add_radio "http://radiodreyeckland.ice.infomaniak.ch/radiodreyeckland-128.mp3" "Dreyeckland" "fr_FR"
add_radio "http://stream1.evasionfm.com/Paris" "Evasion Paris" "fr_FR"
add_radio "http://stream1.evasionfm.com/Essonne" "Evasion Essonne" "fr_FR"
add_radio "http://stream1.evasionfm.com/Nord77" "Evasion Nord 77" "fr_FR"
add_radio "http://stream1.evasionfm.com/Oise" "Evasion Oise" "fr_FR"
add_radio "http://stream1.evasionfm.com/Somme" "Evasion Somme" "fr_FR"
add_radio "http://stream1.evasionfm.com/Sud77" "Evasion Sud 77" "fr_FR"
add_radio "http://stream1.evasionfm.com/Yvelines" "Evasion Yvelines" "fr_FR"
add_radio "http://fcradio.ice.infomaniak.ch/fcradio-128.mp3" "FC Radio" "fr_FR"
add_radio "http://fm43.ice.infomaniak.ch/fm43-128.mp3" "FM 43" "fr_FR"
add_radio "http://start-forum.ice.infomaniak.ch/start-forum-high.mp3" "Forum" "fr_FR"
add_radio "http://freqplus.ice.infomaniak.ch/freqplus-high.mp3" "Frequence Plus" "fr_FR"
add_radio "http://generationfm.ice.infomaniak.ch/generationfm-high.mp3" "Generations" "fr_FR"
add_radio "http://broadcast.infomaniak.ch/hitwest-high.mp3" "Hit West" "fr_FR"
add_radio "http://live.jordannefm.com:8000/JFMCantalMD.mp3" "Jordanne FM" "fr_FR"
add_radio "http://laradioplus.ice.infomaniak.ch/laradioplus-high.mp3" "La Radio Plus Alpes du nord Suisse" "fr_FR"
add_radio "http://lalaradio.ice.infomaniak.ch/lalaradio-high.mp3" "La Radio Plus Alpes du sud" "fr_FR"
add_radio "http://str0.creacast.com/magnum" "Magnum La Radio" "fr_FR"
add_radio "http://monafm.bcast.infomaniak.ch/monafm-high.mp3" "Mona FM" "fr_FR"
add_radio "http://montagnefm.ice.infomaniak.ch/montagnefm-96.mp3" "Montagne FM" "fr_FR"
add_radio "http://nradio.ice.infomaniak.ch/nradio-128.mp3" "N Radio" "fr_FR"
add_radio "http://ods.ice.infomaniak.ch/ods-high.mp3" "ODS Radio" "fr_FR"
add_radio "http://ouestfm.ice.infomaniak.ch/ouestfm-high.mp3" "Ouest FM" "fr_FR"
add_radio "http://antenne.pyreneesfm.com/;stream/1" "Pyrenees FM" "fr_FR"
add_radio "http://direct.radiocourtoisie.fr" "Radio Courtoisie" "fr_FR"
add_radio "http://radiocristal.ice.infomaniak.ch/radiocristal-high.mp3" "Radio Cristal" "fr_FR"
add_radio "http://radioespace.ice.infomaniak.ch/radioespace-high.mp3" "Radio Espace" "fr_FR"
add_radio "http://stream.radio-esperance.net/esperance.mp3" "Radio Esperance" "fr_FR"
add_radio "http://radioisagrenoble.ice.infomaniak.ch/radioisagrenoble-128.mp3" "Radio Isa Grenoble" "fr_FR"
add_radio "http://radioisa.ice.infomaniak.ch/radioisa-128.mp3" "Radio Isa Nord Isere" "fr_FR"
add_radio "http://str0.creacast.com/metropolys" "Radio Metropolys" "fr_FR"
add_radio "http://radiono1.ice.infomaniak.ch/radiono1-128.mp3" "Radio Numero 1" "fr_FR"
add_radio "http://radioscoop-bourg.ice.infomaniak.ch:80/radioscoop-bourg-128" "Radio Scoop Bourg en Bresse" "fr_FR"
add_radio "http://radioscoop-clermont.ice.infomaniak.ch:80/radioscoop-clermont-128" "Radio Scoop Clermont Ferrand" "fr_FR"
add_radio "http://radioscoop-lyon.ice.infomaniak.ch:80/radioscoop-lyon-128" "Radio Scoop Lyon" "fr_FR"
add_radio "http://radioscoop-stetienne.ice.infomaniak.ch:80/radioscoop-stetienne-128" "Radio Scoop Saint Etienne" "fr_FR"
add_radio "http://radiostar.ice.infomaniak.ch/radiostar-128.mp3" "Radio Star Franche Comte" "fr_FR"
add_radio "https://listen.radioking.com/radio/9728/stream/20210" "Radio Star PACA" "fr_FR"
add_radio "http://radiosun.ice.infomaniak.ch/radiosun-high.mp3" "Radio Sun" "fr_FR"
add_radio "http://stream.rtsfm.com/rts-national.mp3" "RTS" "fr_FR"
add_radio "http://rva.ice.infomaniak.ch/rva-high.mp3" "RVA" "fr_FR"
add_radio "http://sensationsessonne.ice.infomaniak.ch/sensationsessonne-128.mp3" "Sensations Essonne" "fr_FR"
add_radio "http://sensationsnormandie.ice.infomaniak.ch/sensations-normandie.mp3" "Sensations Normandie Bernay" "fr_FR"
add_radio "http://sensations.ice.infomaniak.ch/sensations.mp3" "Sensations Yvelines" "fr_FR"
add_radio "http://62.210.214.169:8000/alc" "Sweet FM Alencon" "fr_FR"
add_radio "http://62.210.214.169:8000/cgr" "Sweet FM Craon" "fr_FR"
add_radio "http://62.210.214.169:8000/aig" "Sweet FM L aigle" "fr_FR"
add_radio "http://62.210.214.169:8000/lms" "Sweet FM Le Mans" "fr_FR"
add_radio "http://62.210.214.169:8000/mms" "Sweet FM Mamers" "fr_FR"
add_radio "http://62.210.214.169:8000/perche" "Sweet FM Saint Calais" "fr_FR"
add_radio "http://radio6-boulognesurmer.ice.infomaniak.ch/radio6-boulognesurmer-128.mp3" "Radio 6 Boulogne sur Mer" "fr_FR"
add_radio "http://radio6-calais.ice.infomaniak.ch/radio6-calais-128.mp3" "Radio 6 Calais" "fr_FR"
add_radio "http://radio6-dunkerque.ice.infomaniak.ch/radio6-dunkerque-128.mp3" "Radio 6 Dunkerque" "fr_FR"
add_radio "http://radio6-montreuilsurmer.ice.infomaniak.ch/radio6-montreuilsurmer-128.mp3" "Radio 6 Montreuil sur Mer" "fr_FR"
add_radio "http://streaming.tendanceouest.com/tomanche.mp3" "Tendance Ouest Manche" "fr_FR"
add_radio "http://streaming.tendanceouest.com/toorne.mp3" "Tendance Ouest Orne" "fr_FR"
add_radio "http://str0.creacast.com/topmusic2" "Top Music Colmar" "fr_FR"
add_radio "http://str0.creacast.com/topmusic3" "Top Music Haguenau" "fr_FR"
add_radio "http://str0.creacast.com/topmusic4" "Top Music Sarrebourg" "fr_FR"
add_radio "http://str0.creacast.com/topmusic5" "Top Music Saverne" "fr_FR"
add_radio "http://str0.creacast.com/topmusic6" "Top Music Selestat" "fr_FR"
add_radio "http://str0.creacast.com/topmusic1" "Top Music Strasbourg" "fr_FR"
add_radio "http://vibration.ice.infomaniak.ch/vibration-high.mp3" "Vibration" "fr_FR"
add_radio "http://virageradio.ice.infomaniak.ch/virageradio-high.mp3" "Virage Radio" "fr_FR"
add_radio "http://start-voltage.ice.infomaniak.ch/start-voltage-high.mp3" "Voltage" "fr_FR"
add_radio "http://start-witfm.ice.infomaniak.ch/start-witfm-high.mp3" "Wit FM" "fr_FR"
add_radio "http://100radio-albi.ice.infomaniak.ch/100radio-albi-128.mp3" "100 Albi" "fr_FR"
add_radio "http://100radio-auch.ice.infomaniak.ch/100radio-auch-128.mp3" "100 Auch" "fr_FR"
add_radio "http://100radio-carcassonne.ice.infomaniak.ch/100radio-carcassonne-128.mp3" "100 Carcassone" "fr_FR"
add_radio "http://100radio-castres.ice.infomaniak.ch/100radio-castres-128.mp3" "100 Castres" "fr_FR"
add_radio "http://100radio-foix.ice.infomaniak.ch/100radio-foix-128.mp3" "100 Foix" "fr_FR"
add_radio "http://100radio-montauban.ice.infomaniak.ch/100radio-montauban-128.mp3" "100 Montauban" "fr_FR"
add_radio "http://100radio-pau.ice.infomaniak.ch/100radio-pau-128.mp3" "100 Pau" "fr_FR"
add_radio "http://100radio-stgaudens.ice.infomaniak.ch/100radio-stgaudens-128.mp3" "100 Saint Gaudens" "fr_FR"
add_radio "http://100radio-tarbes.ice.infomaniak.ch/100radio-tarbes-128.mp3" "100 Tarbes" "fr_FR"

# Trafic
add_radio "http://media.autorouteinfo.fr:8000/direct_nord.mp3" "Autoroute Info Zone Nord" "fr_FR"
add_radio "http://media.autorouteinfo.fr:8000/direct_sud.mp3" "Autoroute Info Zone Sud" "fr_FR"
add_radio "http://str0.creacast.com/radio_vinci_autoroutes_6" "Radio Vinci Autoroutes Zone Alpes Provence" "fr_FR"
add_radio "http://str0.creacast.com/radio_vinci_autoroutes_5" "Radio Vinci Autoroutes Zone Auvergne Zone Vallee du Rhone" "fr_FR"
add_radio "http://str0.creacast.com/radio_vinci_autoroutes_7" "Radio Vinci Autoroutes Zone Cote d Azur" "fr_FR"
add_radio "http://str0.creacast.com/radio_vinci_autoroutes_2" "Radio Vinci Autoroutes Zone Grand Ouest" "fr_FR"
add_radio "http://str0.creacast.com/radio_vinci_autoroutes_4" "Radio Vinci Autoroutes Zone Languedoc Roussillon" "fr_FR"
add_radio "http://str0.creacast.com/radio_vinci_autoroutes_1" "Radio Vinci Autoroutes Zone Ouest Centre" "fr_FR"
add_radio "http://str0.creacast.com/radio_vinci_autoroutes_3" "Radio Vinci Autoroutes Zone Sud Ouest" "fr_FR"
add_radio "http://str0.creacast.com/radio_vinci_autoroutes_8" "Radio Vinci Autoroutes Zone Toulouse" "fr_FR"
add_radio "http://mediaming.streamakaci.com/sanef107_7_EST.mp3" "Sanef 107 7 Zone Est" "fr_FR"
add_radio "http://mediaming.streamakaci.com/sanef107_7_NORD.mp3" "Sanef 107 7 Zone Nord" "fr_FR"
add_radio "http://mediaming.streamakaci.com/sanef107_7_OUEST.mp3" "Sanef 107 7 Zone Ouest" "fr_FR"
add_radio "http://206.190.150.92:8399/;" "Radio Transat Saint Martin" "fr_FR"
add_radio "http://sv1.vestaradio.com:4330/;" "Radio Tropik FM" "fr_FR"
add_radio "http://radios.la1ere.fr/guadeloupe" "Guadeloupe 1ere" "fr_FR"
add_radio "http://167.114.0.166:8000/massabielle.mp3" "Radio Massabielle" "fr_FR"
add_radio "http://2stream.dyndns.org:8000/rsb" "Radio Saint Barth FM" "fr_FR"
add_radio "https://stream.sosradio959.com/" "Radio SOS" "fr_FR"
add_radio "http://206.190.150.92:8399/;" "Radio Transat" "fr_FR"
add_radio "http://direct.franceinter.fr/live/franceinter-midfi.mp3" "France Inter" "fr_FR"
add_radio "http://radios.la1ere.fr/wallisfutuna" "Wallis et Futuna 1ere" "fr_FR"
add_radio "http://212.83.172.22/radio/58216/stream/95443" "NRJ Tahiti" "fr_FR"
add_radio "http://radios.la1ere.fr/polynesie" "Polynesie 1ere" "fr_FR"
add_radio "http://live.radio1.pf:8000/radio1" "Radio 1" "fr_FR"
add_radio "http://live.mcrmedia.net:8014/mcr2.mp3" "Radio la Voix de l Esperance" "fr_FR"
add_radio "http://s24.myradiostream.com:5698/;listen.mp3" "Radio Maohi" "fr_FR"
add_radio "https://stric6.streamakaci.com/radiomarianotehau.mp3" "Radio Maria No Te Hau" "fr_FR"
add_radio "http://live.radio1.pf:8000/tiarefm" "Radio Tiare FM" "fr_FR"
add_radio "http://195.154.156.183/radio/60268/stream/97597" "Rire et Chansons Tahiti" "fr_FR"
add_radio "http://hosting-serv.com:9200/" "Taui FM" "fr_FR"
add_radio "http://radios.la1ere.fr/nouvellecaledonie" "Nouvelle Caledonie 1ere" "fr_FR"
add_radio "http://radio.lagoon.nc/nrj" "NRJ New Caledonia" "fr_FR"
add_radio "http://radiodjiido.nc:8000" "Radio Djiido" "fr_FR"
add_radio "http://radio.oceanefm.nc:8000/oceanenc" "Radio Oceane FM" "fr_FR"
add_radio "http://radio.lagoon.nc/rrb" "Radio Rythme Bleu RRB" "fr_FR"

# fr_BE
add_radio "http://audiostream.rtl.be/belrtl128" "Bel RTL" "fr_BE"
add_radio "http://streamingp.shoutcast.com/Cherie-mp3" "Cherie FM Belgique" "fr_BE"
add_radio "https://radios.rtbf.be/classic21-128.mp3" "Classic 21" "fr_BE"
add_radio "http://dhradio.ice.infomaniak.ch/dhradio-192.mp3" "DH Radio" "fr_BE"
add_radio "http://live.funradio.be/funradiobe-high.mp3" "Fun Radio Belgique" "fr_BE"
add_radio "https://radios.rtbf.be/laprem1erebxl-128.mp3" "La Premiere Bruxelles" "fr_BE"
add_radio "https://radios.rtbf.be/laprem1ere-128.mp3" "La Premiere Wallonie" "fr_BE"
add_radio "http://radios.rtbf.be/musiclab-128.mp3" "Jam" "fr_BE"
add_radio "https://radios.rtbf.be/musiq3-128.mp3" "Musiq 3" "fr_BE"
add_radio "http://streamingp.shoutcast.com/NostalgiePremium-mp3" "Nostalgie Belgique" "fr_BE"
add_radio "http://streamingp.shoutcast.com/NRJ" "NRJ Belgique" "fr_BE"
add_radio "https://radios.rtbf.be/pure-128.mp3" "Pure FM" "fr_BE"
add_radio "http://audiostream.rtl.be/contactfr192" "Radio Contact" "fr_BE"
add_radio "https://radios.rtbf.be/rtbfmix-128.mp3" "RTBF Mix" "fr_BE"
add_radio "https://radios.rtbf.be/tarmac-128.mp3" "Tarmac" "fr_BE"
add_radio "http://radios.rtbf.be/vivaplus-128.mp3" "Viva Plus" "fr_BE"
add_radio "https://radios.rtbf.be/vivabw-128.mp3" "VivaCite Brabant Wallon" "fr_BE"
add_radio "https://radios.rtbf.be/vivabxl-128.mp3" "VivaCite Bruxelles" "fr_BE"
add_radio "https://radios.rtbf.be/vivacharleroi-128.mp3" "VivaCite Charleroi" "fr_BE"
add_radio "https://radios.rtbf.be/vivahainaut-128.mp3" "VivaCite Hainaut" "fr_BE"
add_radio "https://radios.rtbf.be/vivaliege-128.mp3" "VivaCite Liege" "fr_BE"
add_radio "https://radios.rtbf.be/vivalux-128.mp3" "VivaCite Luxembourg" "fr_BE"
add_radio "https://radios.rtbf.be/vivanamurbw-128.mp3" "VivaCite Namur" "fr_BE"
add_radio "https://radios.rtbf.be/rtbfmix-128.mp3" "1R C F Belgique" "fr_BE"
add_radio "http://arabelfm.ice.infomaniak.ch/arabelprodcastfm.mp3" "Arabel" "fr_BE"
add_radio "https://bxfmradio.ice.infomaniak.ch/bxfmradio-192.mp3" "BXFM" "fr_BE"
add_radio "http://onair.goldfm.be:1061/goldfm.mp3" "Gold FM" "fr_BE"
add_radio "http://live.kif.be/kif_mp3" "KIF" "fr_BE"
add_radio "http://streaming.domainepublic.net:8000/radioairlibre.ogg" "Radio Air Libre" "fr_BE"
add_radio "http://shoutcast2.wirelessbelgie.be:8310/;stream.mp3" "Radio Alma" "fr_BE"
add_radio "http://streamer.radiocampus.be:8000/stream.mp3" "Radio Campus" "fr_BE"
add_radio "http://radiojudaica.ice.infomaniak.ch/radiojudaica-128.mp3" "Radio Judaica" "fr_BE"
add_radio "http://streaming.domainepublic.net:8000/radiopanik.ogg" "Radio Panik" "fr_BE"
add_radio "http://rcf.streamakaci.com/rcfbruxelles.mp3" "R C F Bruxelles" "fr_BE"
add_radio "http://vibrationbelgique.ice.infomaniak.ch/vibrationbelgique-high" "Vibration Bruxelles" "fr_BE"
add_radio "http://antipode.belstream.net/antipode128.mp3" "Antipode" "fr_BE"
add_radio "http://87.98.189.101:80/" "Emotion" "fr_BE"
add_radio "http://streaming.domainepublic.net:8000/lnfm.ogg" "LouiZ Radio" "fr_BE"
add_radio "http://france1.coollabel-productions.com:2199/tunein/radiopassionfm173.asx" "Passion FM" "fr_BE"
add_radio "http://ultrason.ice.infomaniak.ch/ultrason-high.mp3" "Ultrason" "fr_BE"
add_radio "http://upradio.ovh:8034/;stream/1" "Up Radio" "fr_BE"
add_radio "http://buzzradio.ice.infomaniak.ch/buzzradio-128.mp3" "Buzz Radio" "fr_BE"
add_radio "http://charlekingradio.ice.infomaniak.ch/charlekingradio.mp3" "Charleking" "fr_BE"
add_radio "http://listen.radioking.com/radio/4732/stream/91750" "C Rap" "fr_BE"
add_radio "http://flashfm.ice.infomaniak.ch/flashfm-128.mp3" "Flash FM" "fr_BE"
add_radio "http://liveflashfm.ice.infomaniak.ch/liveflashfm-128.mp3" "Generation" "fr_BE"
add_radio "http://91.121.29.128:8106/;" "J600" "fr_BE"
add_radio "http://lecentrefm.ice.infomaniak.ch/lecentrefm.mp3.m3u" "Le Centre FM" "fr_BE"
add_radio "http://live.libellulefm.com:8000/stream" "Libellule FM" "fr_BE"
add_radio "http://188.165.35.60:7150/" "Ma Radio" "fr_BE"
add_radio "http://ks393340.kimsufi.com:9290/;" "Max FM" "fr_BE"
add_radio "http://streammelodie.dgnet.be:8008" "Melodie FM" "fr_BE"
add_radio "https://mixxfm.ice.infomaniak.ch/mixxfm-192.mp3" "Mixx FM" "fr_BE"
add_radio "http://8500.go2stream.fr:8000/stream" "Neo Radio" "fr_BE"
add_radio "http://vt-net.org:8072/;" "Pacifique FM" "fr_BE"
add_radio "http://str0.creacast.com/pharefm_mons" "Phare FM Mons" "fr_BE"
add_radio "http://france1.coollabel-productions.com:2199/tunein/radiobonheur317.pls" "Radio Bonheur" "fr_BE"
add_radio "http://radiomusicsambre.ice.infomaniak.ch/radiomusicsambre-128.mp3" "Radio Music Sambre RMS" "fr_BE"
add_radio "http://ks393340.kimsufi.com:9850/;stream.mp3" "Radio Stars" "fr_BE"
add_radio "http://163.172.96.134:8156/;" "Ramdam Musique" "fr_BE"
add_radio "http://5.39.80.55:8000/live;" "Radio qui Chifel" "fr_BE"
add_radio "http://sudradio.ice.infomaniak.ch/sudradio-96.aac" "Sud Radio" "fr_BE"
add_radio "http://94.23.26.193:9361/strem" "Vivante FM" "fr_BE"
add_radio "http://youfm.ice.infomaniak.ch/youfm-96.aac" "YouFM" "fr_BE"
add_radio "http://139.165.3.181:8000/48fm" "48FM" "fr_BE"
add_radio "http://equinoxefm.ddns.net:8000/stream.ogg" "Equinoxe FM" "fr_BE"
add_radio "http://impactfm2.ice.infomaniak.ch/impactfmbe-64.aac" "Impact FM" "fr_BE"
add_radio "http://maximum.belstream.net/maximum.mp3" "Maximum FM" "fr_BE"
add_radio "http://onair.turkuazfm.be:1018/stream" "Panach FM" "fr_BE"
add_radio "http://94.23.221.158:9073/stream2" "Radio 4910 " "fr_BE"
add_radio "http://wma02.fluidstream.net:8130/;stream.nsv" "Radio Hitalia" "fr_BE"
add_radio "http://roa1062fm.ice.infomaniak.ch/roa1062fm-128.mp3" "Radio Ourthe Ambleve" "fr_BE"
add_radio "http://manager2.streaming-ingenierie.fr:8016/stream" "Radio Plus" "fr_BE"
add_radio "http://37.187.114.62:8002/live" "Radio Prima" "fr_BE"
add_radio "http://rcf.streamakaci.com/rcfliege.mp3" "R C F Liege" "fr_BE"
add_radio "http://onair.turkuazfm.be:1078/stream" "Turkuaz FM " "fr_BE"
add_radio "http://stream01.warm.fm:9002/warm128k" "Warm FM" "fr_BE"
add_radio "http://frequenceeghezee.ice.infomaniak.ch/frequenceeghezee-128.mp3" "Frequence Eghezee" "fr_BE"
add_radio "http://frequenceplusandenne.ice.infomaniak.ch/frequenceplusandenne-192.mp3" "Frequence Plus" "fr_BE"
add_radio "http://94.23.31.72:8000/hitradio.mp3" "Hit Radio Namur" "fr_BE"
add_radio "http://mustfm.ice.infomaniak.ch/mustfm-192.mp3" "Must FM" "fr_BE"
add_radio "http://equinoxefm.ddns.net:8000/stream.ogg" "Radio Equinoxe" "fr_BE"
add_radio "http://radiomusicsambre.ice.infomaniak.ch/radiomusicsambre-128.mp3" "Radio Music Sambre RMS" "fr_BE"
add_radio "http://manager2.streaming-ingenierie.fr:8026/;stream.mp3" "Radio Quartz" "fr_BE"
add_radio "http://studiooneasbl.ice.infomaniak.ch/studiooneasbl-128.mp3" "Radio Studio One" "fr_BE"
add_radio "http://stream.run.be:8000/run128.mp3" "Radio Universitaire Namuroise RUN" "fr_BE"
add_radio "http://snoupyfm1069.ice.infomaniak.ch/snoupyfm1069-64.mp3" "Snoupy FM" "fr_BE"
add_radio "http://7fm2.ice.infomaniak.ch/7fm2.mp3" "7FM" "fr_BE"
add_radio "http://maxpag.eu:8000/metropole" "Metropole Radio" "fr_BE"
add_radio "http://mustfm.ice.infomaniak.ch/mustfm-192.mp3" "Must FM" "fr_BE"
add_radio "http://radiosud.ice.infomaniak.ch/radiosud-128.mp3" "Radio Sud" "fr_BE"
add_radio "http://rcf.streamakaci.com/rcfsudbe.mp3" "R C F Sud Belgique" "fr_BE"
add_radio "http://94.23.27.6:8286/;stream.mp3" "RLO Radio" "fr_BE"

# fr_LU
add_radio "https://stream.antenne-luxemburg.lu/radio/8000/192-mp3" "Antenne Luxemburg" "fr_LU"
add_radio "http://81.92.238.33:80/" "Eldoradio" "fr_LU"
add_radio "http://sc-eldo80s.newmedia.lu:80/" "Eldoradio 80s Channel" "fr_LU"
add_radio "http://sc-eldo90s.newmedia.lu/;*.mp3" "Eldoradio 90s Channel" "fr_LU"
add_radio "http://sc-eldoalt.newmedia.lu:80/" "Eldoradio Alternative Channel" "fr_LU"
add_radio "http://sc-eldochill.newmedia.lu:80/" "Eldoradio Chill Channel" "fr_LU"
add_radio "http://sc-eldotop25.newmedia.lu:80/" "Eldoradio Top 25 Channel" "fr_LU"
add_radio "http://lessentielradio.ice.infomaniak.ch/lessentielradio-128.mp3" "L Essentiel Radio" "fr_LU"
add_radio "http://100komma7.cast.addradio.de/100komma7/live/mp3/128/stream.mp3" "Radio 100 7" "fr_LU"
add_radio "http://zeus.lrb.lu:8000/lrb.live" "LRB" "fr_LU"
add_radio "http://zeus.lrb.lu:8000/stream" "Radio Aktiv" "fr_LU"
add_radio "http://stream.ara.lu/stream.mp3" "Radio ARA" "fr_LU"
add_radio "http://radio.rbv.lu:8002/;stream.mp3" "Radio Belle Vallee" "fr_LU"
add_radio "http://radiodudelange.ice.infomaniak.ch/radiodudelange-128.mp3" "Radio Diddeleng" "fr_LU"
add_radio "https://stream.latinaplayer.wort.lu:9443/stream" "Radio Latina" "fr_LU"
add_radio "mms://213.135.229.13:80/" "Radio LNW" "fr_LU"
add_radio "http://rs1.radiostreamer.com:8270/;" "ROM" "fr_LU"
add_radio "http://sc-rtllive.newmedia.lu/;stream.nvs" "RTL Radio Letzebuerg" "fr_LU"

# Webradio
add_radio "http://streamingads.hotmixradio.fm/hotmixradio-80-128.mp3" "Hotmix Radio 80s" "Web"
add_radio "http://streamingads.hotmixradio.fm/hotmixradio-90-128.mp3" "Hotmix Radio 90s" "Web"
add_radio "http://streamingads.hotmixradio.fm/hotmixradio-2k-128.mp3" "Hotmix Radio 2000s" "Web"
add_radio "http://streamingads.hotmixradio.fm/hotmixradio-baby-128.mp3" "Hotmix Radio Baby" "Web"
add_radio "http://streamingads.hotmixradio.fm/hotmixradio-dance-128.mp3" "Hotmix Radio Dance" "Web"
add_radio "http://streamingads.hotmixradio.fm/hotmixradio-frenchy-128.mp3" "Hotmix Radio Frenchy" "Web"
add_radio "http://streamingads.hotmixradio.fm/hotmixradio-funky-128.mp3" "Hotmix Radio Funky" "Web"
add_radio "http://streamingads.hotmixradio.fm/hotmixradio-game-128.mp3" "Hotmix Radio Game by Laser Game Evolution" "Web"
add_radio "http://streamingads.hotmixradio.fm/hotmixradio-golds-128.mp3" "Hotmix Radio Golds" "Web"
add_radio "http://streamingads.hotmixradio.fm/hotmixradio-hiphop-128.mp3" "Hotmix Radio Hip Hop" "Web"
add_radio "http://streamingads.hotmixradio.fm/hotmixradio-hits-128.mp3" "Hotmix Radio Hits" "Web"
add_radio "http://streamingads.hotmixradio.fm/hotmixradio-hot-128.mp3" "Hotmix Radio Hot" "Web"
add_radio "http://streamingads.hotmixradio.fm/hotmixradio-japan-128.mp3" "Hotmix Radio Japan" "Web"
add_radio "http://streamingads.hotmixradio.fm/hotmixradio-lounge-128.mp3" "Hotmix Radio Lounge" "Web"
add_radio "http://streamingads.hotmixradio.fm/hotmixradio-metal-128.mp3" "Hotmix Radio Metal" "Web"
add_radio "http://streamingads.hotmixradio.fm/hotmixradio-new-128.mp3" "Hotmix Radio New" "Web"
add_radio "http://streamingads.hotmixradio.fm/hotmixradio-rock-128.mp3" "Hotmix Radio Rock" "Web"
add_radio "http://streamingads.hotmixradio.fm/hotmixradio-sunny-128.mp3" "Hotmix Radio Sunny" "Web"
add_radio "http://streamingads.hotmixradio.fm/hotmixradio-vip-128.mp3" "Hotmix Radio VIP" "Web"

radio() {
  CHOISE=$($DIALOG --clear --title "radio.sh" \
    --cancel-button "Quit" --ok-button "Listen" \
  	--menu "Choose a radio :" 40 80 38 \
    "France Info" "fr_FR" \
    "France Inter" "fr_FR" \
    "France Musique" "fr_FR" \
    "FIP" "fr_FR" \
    "Le Mouv" "fr_FR" \
    "France Culture" "fr_FR" \
    "France Bleu" "fr_FR" \
    "Jazz Radio" "fr_FR" \
    "NRJ" "fr_FR" \
    "Nostalgie" "fr_FR" \
    "Cherie FM" "fr_FR" \
    "Rire et Chansons" "fr_FR" \
    "RTL" "fr_FR" \
    "RTL2" "fr_FR" \
    "Fun Radio" "fr_FR" \
    "Europe 1" "fr_FR" \
    "Virgin Radio" "fr_FR" \
    "RFM" "fr_FR" \
    "RMC" "fr_FR" \
    "BFM Business" "fr_FR" \
    "Skyrock" "fr_FR" \
    "Radio Classique" "fr_FR" \
    "Oui FM" "fr_FR" \
    "M Radio" "fr_FR" \
    "France Maghreb 2" "fr_FR" \
    "Latina" "fr_FR" \
    "TSF Jazz" "fr_FR" \
    "Nova" "fr_FR" \
    "Phare FM" "fr_FR" \
    "Radio FG" "fr_FR" \
    "RFC" "fr_FR" \
    "RFI Monde" "fr_FR" \
    "Sud Radio" "fr_FR" \
    "Ado FM" "fr_FR" \
    "Africa N1" "fr_FR" \
    "Alouette" "fr_FR" \
    "Alpes 1 Grand Grenoble" "fr_FR" \
    "Alpes 1 Alpes du Sud" "fr_FR" \
    "Alta Frequenza" "fr_FR" \
    "Beur FM" "fr_FR" \
    "Blackbox" "fr_FR" \
    "Champagne FM Aisne" "fr_FR" \
    "Champagne FM Ardennes" "fr_FR" \
    "Champagne FM Aude" "fr_FR" \
    "Chante France" "fr_FR" \
    "Contact FM" "fr_FR" \
    "Dreyeckland" "fr_FR" \
    "Evasion Paris" "fr_FR" \
    "Evasion Essonne" "fr_FR" \
    "Evasion Nord 77" "fr_FR" \
    "Evasion Oise" "fr_FR" \
    "Evasion Somme" "fr_FR" \
    "Evasion Sud 77" "fr_FR" \
    "Evasion Yvelines" "fr_FR" \
    "FC Radio" "fr_FR" \
    "FM 43" "fr_FR" \
    "Forum" "fr_FR" \
    "Frequence Plus" "fr_FR" \
    "Generations" "fr_FR" \
    "Hit West" "fr_FR" \
    "Jordanne FM" "fr_FR" \
    "La Radio Plus Alpes du nord Suisse" "fr_FR" \
    "La Radio Plus Alpes du sud" "fr_FR" \
    "Magnum La Radio" "fr_FR" \
    "Mona FM" "fr_FR" \
    "Montagne FM" "fr_FR" \
    "N Radio" "fr_FR" \
    "ODS Radio" "fr_FR" \
    "Ouest FM" "fr_FR" \
    "Pyrenees FM" "fr_FR" \
    "Radio Courtoisie" "fr_FR" \
    "Radio Cristal" "fr_FR" \
    "Radio Espace" "fr_FR" \
    "Radio Esperance" "fr_FR" \
    "Radio Isa Grenoble" "fr_FR" \
    "Radio Isa Nord Isere" "fr_FR" \
    "Radio Metropolys" "fr_FR" \
    "Radio Numero 1" "fr_FR" \
    "Radio Scoop Bourg en Bresse" "fr_FR" \
    "Radio Scoop Clermont Ferrand" "fr_FR" \
    "Radio Scoop Lyon" "fr_FR" \
    "Radio Scoop Saint Etienne" "fr_FR" \
    "Radio Star Franche Comte" "fr_FR" \
    "Radio Star PACA" "fr_FR" \
    "Radio Sun" "fr_FR" \
    "RTS" "fr_FR" \
    "RVA" "fr_FR" \
    "Sensations Essonne" "fr_FR" \
    "Sensations Normandie Bernay" "fr_FR" \
    "Sensations Yvelines" "fr_FR" \
    "Sweet FM Alencon" "fr_FR" \
    "Sweet FM Craon" "fr_FR" \
    "Sweet FM L aigle" "fr_FR" \
    "Sweet FM Le Mans" "fr_FR" \
    "Sweet FM Mamers" "fr_FR" \
    "Sweet FM Saint Calais" "fr_FR" \
    "Radio 6 Boulogne sur Mer" "fr_FR" \
    "Radio 6 Calais" "fr_FR" \
    "Radio 6 Dunkerque" "fr_FR" \
    "Radio 6 Montreuil sur Mer" "fr_FR" \
    "Tendance Ouest Manche" "fr_FR" \
    "Tendance Ouest Orne" "fr_FR" \
    "Top Music Colmar" "fr_FR" \
    "Top Music Haguenau" "fr_FR" \
    "Top Music Sarrebourg" "fr_FR" \
    "Top Music Saverne" "fr_FR" \
    "Top Music Selestat" "fr_FR" \
    "Top Music Strasbourg" "fr_FR" \
    "Vibration" "fr_FR" \
    "Virage Radio" "fr_FR" \
    "Voltage" "fr_FR" \
    "Wit FM" "fr_FR" \
    "100 Albi" "fr_FR" \
    "100 Auch" "fr_FR" \
    "100 Carcassone" "fr_FR" \
    "100 Castres" "fr_FR" \
    "100 Foix" "fr_FR" \
    "100 Montauban" "fr_FR" \
    "100 Pau" "fr_FR" \
    "100 Saint Gaudens" "fr_FR" \
    "100 Tarbes" "fr_FR" \
    "Autoroute Info Zone Nord" "fr_FR" \
    "Autoroute Info Zone Sud" "fr_FR" \
    "Radio Vinci Autoroutes Zone Alpes Provence" "fr_FR" \
    "Radio Vinci Autoroutes Zone Auvergne Zone Vallee du Rhone" "fr_FR" \
    "Radio Vinci Autoroutes Zone Cote d Azur" "fr_FR" \
    "Radio Vinci Autoroutes Zone Grand Ouest" "fr_FR" \
    "Radio Vinci Autoroutes Zone Languedoc Roussillon" "fr_FR" \
    "Radio Vinci Autoroutes Zone Ouest Centre" "fr_FR" \
    "Radio Vinci Autoroutes Zone Sud Ouest" "fr_FR" \
    "Radio Vinci Autoroutes Zone Toulouse" "fr_FR" \
    "Sanef 107 7 Zone Est" "fr_FR" \
    "Sanef 107 7 Zone Nord" "fr_FR" \
    "Sanef 107 7 Zone Ouest" "fr_FR" \
    "Radio Transat Saint Martin" "fr_FR" \
    "Radio Tropik FM" "fr_FR" \
    "Guadeloupe 1ere" "fr_FR" \
    "Radio Massabielle" "fr_FR" \
    "Radio Saint Barth FM" "fr_FR" \
    "Radio SOS" "fr_FR" \
    "Radio Transat" "fr_FR" \
    "France Inter" "fr_FR" \
    "Wallis et Futuna 1ere" "fr_FR" \
    "NRJ Tahiti" "fr_FR" \
    "Polynesie 1ere" "fr_FR" \
    "Radio 1" "fr_FR" \
    "Radio la Voix de l Esperance" "fr_FR" \
    "Radio Maohi" "fr_FR" \
    "Radio Maria No Te Hau" "fr_FR" \
    "Radio Tiare FM" "fr_FR" \
    "Rire et Chansons Tahiti" "fr_FR" \
    "Taui FM" "fr_FR" \
    "Nouvelle Caledonie 1ere" "fr_FR" \
    "NRJ New Caledonia" "fr_FR" \
    "Radio Djiido" "fr_FR" \
    "Radio Oceane FM" "fr_FR" \
    "Radio Rythme Bleu RRB" "fr_FR" \
    "Bel RTL" "fr_BE" \
    "Cherie FM Belgique" "fr_BE" \
    "Classic 21" "fr_BE" \
    "DH Radio" "fr_BE" \
    "Fun Radio Belgique" "fr_BE" \
    "La Premiere Bruxelles" "fr_BE" \
    "La Premiere Wallonie" "fr_BE" \
    "Jam" "fr_BE" \
    "Musiq 3" "fr_BE" \
    "Nostalgie Belgique" "fr_BE" \
    "NRJ Belgique" "fr_BE" \
    "Pure FM" "fr_BE" \
    "Radio Contact" "fr_BE" \
    "RTBF Mix" "fr_BE" \
    "Tarmac" "fr_BE" \
    "Viva Plus" "fr_BE" \
    "VivaCite Brabant Wallon" "fr_BE" \
    "VivaCite Bruxelles" "fr_BE" \
    "VivaCite Charleroi" "fr_BE" \
    "VivaCite Hainaut" "fr_BE" \
    "VivaCite Liege" "fr_BE" \
    "VivaCite Luxembourg" "fr_BE" \
    "VivaCite Namur" "fr_BE" \
    "1R C F Belgique" "fr_BE" \
    "Arabel" "fr_BE" \
    "BXFM" "fr_BE" \
    "Gold FM" "fr_BE" \
    "KIF" "fr_BE" \
    "Radio Air Libre" "fr_BE" \
    "Radio Alma" "fr_BE" \
    "Radio Campus" "fr_BE" \
    "Radio Judaica" "fr_BE" \
    "Radio Panik" "fr_BE" \
    "R C F Bruxelles" "fr_BE" \
    "Vibration Bruxelles" "fr_BE" \
    "Antipode" "fr_BE" \
    "Emotion" "fr_BE" \
    "LouiZ Radio" "fr_BE" \
    "Passion FM" "fr_BE" \
    "Ultrason" "fr_BE" \
    "Up Radio" "fr_BE" \
    "Buzz Radio" "fr_BE" \
    "Charleking" "fr_BE" \
    "C Rap" "fr_BE" \
    "Flash FM" "fr_BE" \
    "Generation" "fr_BE" \
    "J600" "fr_BE" \
    "Le Centre FM" "fr_BE" \
    "Libellule FM" "fr_BE" \
    "Ma Radio" "fr_BE" \
    "Max FM" "fr_BE" \
    "Melodie FM" "fr_BE" \
    "Mixx FM" "fr_BE" \
    "Neo Radio" "fr_BE" \
    "Pacifique FM" "fr_BE" \
    "Phare FM Mons" "fr_BE" \
    "Radio Bonheur" "fr_BE" \
    "Radio Music Sambre RMS" "fr_BE" \
    "Radio Stars" "fr_BE" \
    "Ramdam Musique" "fr_BE" \
    "Radio qui Chifel" "fr_BE" \
    "Sud Radio" "fr_BE" \
    "Vivante FM" "fr_BE" \
    "YouFM" "fr_BE" \
    "48FM" "fr_BE" \
    "Equinoxe FM" "fr_BE" \
    "Impact FM" "fr_BE" \
    "Maximum FM" "fr_BE" \
    "Panach FM" "fr_BE" \
    "Radio 4910 " "fr_BE" \
    "Radio Hitalia" "fr_BE" \
    "Radio Ourthe Ambleve" "fr_BE" \
    "Radio Plus" "fr_BE" \
    "Radio Prima" "fr_BE" \
    "R C F Liege" "fr_BE" \
    "Turkuaz FM " "fr_BE" \
    "Warm FM" "fr_BE" \
    "Frequence Eghezee" "fr_BE" \
    "Frequence Plus" "fr_BE" \
    "Hit Radio Namur" "fr_BE" \
    "Must FM" "fr_BE" \
    "Radio Equinoxe" "fr_BE" \
    "Radio Music Sambre RMS" "fr_BE" \
    "Radio Quartz" "fr_BE" \
    "Radio Studio One" "fr_BE" \
    "Radio Universitaire Namuroise RUN" "fr_BE" \
    "Snoupy FM" "fr_BE" \
    "7FM" "fr_BE" \
    "Metropole Radio" "fr_BE" \
    "Must FM" "fr_BE" \
    "Radio Sud" "fr_BE" \
    "R C F Sud Belgique" "fr_BE" \
    "RLO Radio" "fr_BE" \
    "Antenne Luxemburg" "fr_LU" \
    "Eldoradio" "fr_LU" \
    "Eldoradio 80s Channel" "fr_LU" \
    "Eldoradio 90s Channel" "fr_LU" \
    "Eldoradio Alternative Channel" "fr_LU" \
    "Eldoradio Chill Channel" "fr_LU" \
    "Eldoradio Top 25 Channel" "fr_LU" \
    "L Essentiel Radio" "fr_LU" \
    "Radio 100 7" "fr_LU" \
    "LRB" "fr_LU" \
    "Radio Aktiv" "fr_LU" \
    "Radio ARA" "fr_LU" \
    "Radio Belle Vallee" "fr_LU" \
    "Radio Diddeleng" "fr_LU" \
    "Radio Latina" "fr_LU" \
    "Radio LNW" "fr_LU" \
    "ROM" "fr_LU" \
    "RTL Radio Letzebuerg" "fr_LU" \
    "Hotmix Radio 80s" "Web" \
    "Hotmix Radio 90s" "Web" \
    "Hotmix Radio 2000s" "Web" \
    "Hotmix Radio Baby" "Web" \
    "Hotmix Radio Dance" "Web" \
    "Hotmix Radio Frenchy" "Web" \
    "Hotmix Radio Funky" "Web" \
    "Hotmix Radio Game by Laser Game Evolution" "Web" \
    "Hotmix Radio Golds" "Web" \
    "Hotmix Radio Hip Hop" "Web" \
    "Hotmix Radio Hits" "Web" \
    "Hotmix Radio Hot" "Web" \
    "Hotmix Radio Japan" "Web" \
    "Hotmix Radio Lounge" "Web" \
    "Hotmix Radio Metal" "Web" \
    "Hotmix Radio New" "Web" \
    "Hotmix Radio Rock" "Web" \
    "Hotmix Radio Sunny" "Web" \
    "Hotmix Radio VIP" "Web" \
    3>&1 1>&2 2>&3)
  RCOD=$?
  case $RCOD in
   0)	echo "'$CHOISE' est votre chanteur français préféré";;
   1) 	echo "Bye folks!"; exit 0 ;;
  255) 	echo "Have a great day!" ; exit 0 ;;
  esac

  __NAME=$(echo "$CHOISE" | sed -e 's/ /_/g')
  URL=$(eval "echo \$RADIO_$__NAME")
  $MPV "$URL" 1>/dev/null 2>/dev/null 3>/dev/null &

  $DIALOG --clear --title "radio.sh" \
    --yes-button "Change" --no-button "Quit" \
    --yesno "You are listening\n$CHOISE" 7 60
  RCOD=$?
  killall -9 $MPV
  case $RCOD in
   0)	radio ;;
   1) echo "Bye folks!"; exit 0 ;;
  255) echo "Have a great day!" ; exit 0 ;;
  esac
}

help() {
  echo "$MAIN_COMMAND v$VERSION"
  echo
  echo "Options:"
  echo
  echo "  --install     Install dependencies"
  echo "  --init        Get init instructions"
  echo "  -h | --help   Display this help"
  echo
}

case "$1" in
  --install)
    check_package "$DIALOG" "dialog"
    check_package "$MPV" "mpv"
    shift
    ;;
  --init)
    shift
    init $1
    shift
    ;;
  --help | -h)
    help
    shift
    ;;
  *)
    radio
    shift
    ;;
esac
